import argparse
import io
import logging
import tokenize
from pathlib import Path
from typing import Iterator
from typing import Optional
from typing import Sequence
from typing import Tuple


logger = logging.getLogger(__name__)

TEST_CLAUSES = [":Given:", ":When:", ":Then:"]


def fixture_docstrings(source: str) -> Iterator[Tuple[str, str]]:
    """
    Generator return tuples of fixture method names and their docstrings

    A fixture is defined as the first method following a line that starts with "@pytest.fixture"

    :param source: String representing the contents of the source python file
    :return:
    * If fixture method has docstring: Tuple of (fixture method name, docstring)
    * If fixture method does not have a docstring: Tuple of (fixture method name, None)
    """
    sio = io.StringIO(source)

    previous_token_string = ""
    previous_token_type = None
    fixture_method_name = None
    next_method_is_fixture = False

    for (token_type, token_string, start, end, line) in tokenize.generate_tokens(sio.readline):

        # Find one of:
        #   Fixture line
        #   Method def line
        #   Docstring

        # Fixture line
        if line.startswith("@pytest.fixture") and token_type == tokenize.NEWLINE:
            next_method_is_fixture = True
            if fixture_method_name:
                # No docstring since the previous fixture method declaration
                yield fixture_method_name, None
                fixture_method_name = None

        # Method definition
        if (
            line.lstrip().startswith("def")
            and previous_token_string == "def"  # Not commented
            and next_method_is_fixture
        ):
            fixture_method_name = token_string.lstrip().split("(")[0]
            next_method_is_fixture = False

        # Docstring
        if (
            token_type == tokenize.STRING
            and token_string.startswith(('"', "'"))
            and previous_token_type == tokenize.INDENT
            and fixture_method_name
        ):
            yield fixture_method_name, token_string
            fixture_method_name = None

        previous_token_string = token_string
        previous_token_type = token_type

    if fixture_method_name:
        yield fixture_method_name, None


def fixturify(source: str) -> int:
    """
    Verifies that docstrings exist for all fixtures in the source string

    :param source: String representing the contents of the source python file
    :return: 0 if file is properly formatted, 1 if it is not
    """
    return_code = 0
    for method, docstring in fixture_docstrings(source):
        if method and not docstring:
            logger.error(f"error: {method} Missing required docstring")
            return_code = 1
        # if method:
        #     # Verify contents of docstring
    return return_code


def has_formatted_clause(text: str, clause: str) -> bool:
    return clause in text


def docstrings_from_tests(source: str) -> Iterator[Tuple[str, str]]:
    """
    Generator returning tuples of test method names and their docstrings

    A test method is defined as a line that starts with optional whitespace followed by "def "
    followed by "test_"

    :param source: String representing the contents of the source python file
    :return: Tuple of (test method name, docstring). Either can be None if the docstring is before
    any tests or a test has no docstring
    """
    sio = io.StringIO(source)

    previous_token_string = ""
    previous_token_type = None
    test_method_name = None

    for (token_type, token_string, start, end, line) in tokenize.generate_tokens(sio.readline):

        # test_ method
        if (previous_token_string == "def") and token_string.startswith("test_"):
            if test_method_name:
                # Docstring has not been found since the previous test_method_name
                logger.error(f"{test_method_name} does not have docstring")
                yield test_method_name, None
            test_method_name = token_string.lstrip().split("(")[0]

        # Docstring
        if (
            token_type == tokenize.STRING
            and token_string.startswith(('"', "'"))
            and (previous_token_type == tokenize.INDENT)
        ):
            yield test_method_name, token_string
            # Reset search for test method
            test_method_name = None

        previous_token_string = token_string
        previous_token_type = token_type

    if test_method_name:
        logger.error(f"Last test method, {test_method_name} does not have a docstring")
        yield test_method_name, None


def given_when_thenify(source: str) -> int:
    """
    Verifies that docstrings exist for all tests in the source string and include the correct
    clauses

    :param source: String representing the contents of the source python file
    :return: 0 if file is properly formatted, 1 if it is not
    """
    logger.debug(f"{source=}")
    return_code = 0
    for method, docstring in docstrings_from_tests(source):
        if not method:
            continue
        if not docstring:
            logger.error(f"error: {method} Missing required docstring")
            return_code = 1
        else:
            for clause in TEST_CLAUSES:
                if not has_formatted_clause(docstring, clause):
                    logger.error(
                        f"error: Docstring for {method} is missing properly formatted clause {clause}"
                    )
                    return_code = 1
    return return_code


def main(argv: Optional[Sequence[str]] = None) -> int:
    """Main entry point for command

    :param argv:
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", nargs="*")
    args = parser.parse_args(argv)
    # TODO: Add ifiers as options

    return_code = 0

    ifiers = [
        given_when_thenify,
        fixturify,
        # TODO: paramify - verify all parameters are doc'ed
        # TODO: ___ify - verify a blank line exists before G/W/T if there is a description and after
        #     G/W/T if there is more text (e.g. params) in the docstring
    ]

    for filename in args.filenames:
        if not filename.endswith(".py"):
            continue
        original_source = Path(filename).read_text()
        formatted_source = original_source
        for ifier in ifiers:
            result = ifier(formatted_source)
            if result != 0:
                logger.error(f"File {filename} failed {ifier.__name__}")
                return_code = 1

    if return_code != 0:
        logger.error(f"formatter failed to validate all files.")

    return return_code


if __name__ == "__main__":
    exit(main())
