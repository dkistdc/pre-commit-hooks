"""
Calibration name pre-commit hook.

This hook verifies that all workflows in the workflows package have corresponding documentation files in the
docs directory and an entry in the index.rst file. If it fails, it logs an error and requires the user to fix the issue.

To find the workflow names, the hook filters files to look only at those in the workflows package in the repo under test.
Those files are parsed using the ast library. If it contains an Assignment node where the target is a call to
Workflow, the node is parsed to find the (poorly named) workflow details needed to generate a consistently formatted
workflow name.

The hook is run by pre-commit and is triggered by the pre-commit configuration file.
"""
import argparse
import ast
import logging
import pathlib
import sys

from dkist_processing_core.workflow import workflow_name_from_details


logger = logging.getLogger(__name__)
HOOK_PASSED = 0
HOOK_FAILED = 1


def get_workflow_names_from_file_contents(
    contents: list[str],
) -> list[str]:
    """Return a list of the workflow names for all the workflows found in the files."""
    workflow_names = []
    for content in contents:
        tree = ast.parse(content)
        for node in ast.walk(tree):
            if isinstance(node, ast.Assign):
                if isinstance(node.value, ast.Call) and node.value.func.id == "Workflow":
                    workflow_name_dict = dict()
                    for item in node.value.keywords:
                        if isinstance(item.value, ast.Constant):
                            workflow_name_dict[item.arg] = item.value.value
                    logger.debug(f"{workflow_name_dict=}")
                    workflow_name = workflow_name_from_details(
                        input_data=workflow_name_dict["input_data"],
                        output_data=workflow_name_dict["output_data"],
                        category=workflow_name_dict["category"],
                        detail=workflow_name_dict.get("detail"),
                    )
                    workflow_names.append(workflow_name)
    return workflow_names


def workflow_missing_from_index(
    workflow_names: list[str],
    index_content: str,
):
    """
    Check if any of the workflow_names are missing from the index file.

    If not, return True and log an error.
    """
    index_lines = [line.strip() for line in index_content.splitlines()]
    missing = False
    for workflow in workflow_names:
        if workflow not in index_lines:
            logger.error(
                f"Workflow {workflow} does not have a corresponding entry in the index file. The workflow must be added to the index file."
            )
            missing = True
    return missing


def workflow_documentation_missing(workflow_names: list[str], doc_files: list[str]):
    """
    Check that all the workflow_names have corresponding documentation files.

    If not return True and log an error.
    """
    missing = False
    for workflow_name in workflow_names:
        if f"{workflow_name}.rst" not in doc_files:
            logger.error(
                f"{workflow_name}.rst not found in docs directory. All workflows must have documentation files."
            )
            missing = True
    return missing


def calibration_workflow_name_hook(filenames: list[str]) -> int:
    """Implement the hook logic."""
    contents = [pathlib.Path(filename).read_text() for filename in filenames]
    workflow_names = get_workflow_names_from_file_contents(contents)

    docs_dir = pathlib.Path.cwd() / "docs"
    if not docs_dir.exists():
        logger.error(f"Documentation {docs_dir} directory not found.")
        return HOOK_FAILED

    doc_files = [path.name for path in docs_dir.glob("*.rst")]

    index_file = docs_dir / "index.rst"
    if not index_file.exists():
        logger.error(f"Documentation index.rst file not found.")
        return HOOK_FAILED
    index_content = (docs_dir / "index.rst").read_text()

    if workflow_documentation_missing(workflow_names, doc_files):
        return HOOK_FAILED
    if workflow_missing_from_index(workflow_names, index_content):
        return HOOK_FAILED

    return HOOK_PASSED


def main() -> int:
    """Entry point for this pre-commit hook. Collect the list of affected files and pass them to the hook logic."""
    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", nargs="*")
    args = parser.parse_args()
    filenames = args.filenames

    return calibration_workflow_name_hook(filenames)


if __name__ == "__main__":
    sys.exit(main())
