import pytest

from pre_commit_hooks.dkistdc_test_formatter import given_when_thenify


def test_valid():
    """
    :Given: Test source code with no errors
    :When: Validated
    :Then: Return code is 0
    """
    assert (
        given_when_thenify(
            '''\
"""Initial docstring""""

def test_valid1():
    """Inline docstring
    :Given: foo
    :When: foo
    :Then: foo
    """
    assert True

def test_valid2():
    """
    Docstring with new line

    :Given: foo
    :When: foo
    :Then: foo
    """
'''
        )
        == 0
    )


def test_has_fixture():
    """
    :Given: Source code with a fixture
    :When: The source is parsed
    :Then: The return code is 0
    """
    assert (
        given_when_thenify(
            """\
@pytest.fixture(scope="function")
def fixture_1() -> str:
    return 1
"""
        )
        == 0
    )


def test_missing_docstring():
    """
    :Given: Source code with a test method that has a missing docstring
    :When: The source is parsed
    :Then: An error code is returned
    :return:
    """
    assert (
        given_when_thenify(
            """\
import foo

def test_1():
    assert True
"""
        )
        == 1
    )


@pytest.mark.parametrize(
    "clauses",
    [
        pytest.param(":Given: foo\n:When: foo", id="Then"),
        pytest.param(":Given: foo\n:Then: foo", id="When"),
        pytest.param(":When: foo\n:Then: foo", id="Given"),
    ],
)
def test_missing_clause(clauses):
    """
    :Given: Source code with a test method is missing a clause in the docstring
    :When: The source is parsed
    :Then: An error code is returned
    """
    assert (
        given_when_thenify(
            f'''\
import foo

def test_1():
    """
{clauses}
    """
    assert True
'''
        )
        == 1
    )


def test_missing_colon():
    """
    :Given: Source code with a test method that a clause in the docstring is not wrapped with a ":"
    :When: The source is parsed
    :Then: An error code is returned
    :return:
    """
    assert given_when_thenify(
        f'''\
import foo

def test_1():
    """
    Given: foo
    When: foo
    Then: foo
    """
    assert True
'''
    )
