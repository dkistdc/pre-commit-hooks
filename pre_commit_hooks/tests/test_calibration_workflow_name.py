"""Tests for the calibration_workflow_name hook."""
import os

import pytest
from dkist_processing_core.workflow import workflow_name_from_details

from pre_commit_hooks.calibration_workflow_name import (
    calibration_workflow_name_hook,
)
from pre_commit_hooks.calibration_workflow_name import (
    get_workflow_names_from_file_contents,
)
from pre_commit_hooks.calibration_workflow_name import (
    workflow_documentation_missing,
)
from pre_commit_hooks.calibration_workflow_name import workflow_missing_from_index


@pytest.fixture
def contents_one_workflow() -> str:
    """Return the contents of a file with a single workflow."""
    return """_ = Workflow(
    input_data="input",
    output_data="output",
    category="category",
    detail="detail"
)"""


@pytest.fixture
def contents_two_workflows() -> str:
    """Return the contents of a file with two workflows."""
    return """_ = Workflow(
    input_data="input1",
    output_data="output1",
    category="category1",
    detail="detail1"
)

_ = Workflow(
    input_data="input2",
    output_data="output2",
    category="category2",
    detail="detail2"
)"""


def test_get_workflow_names_from_files_single_workflow(contents_one_workflow):
    """
    :Given: One code file with a single workflow
    :When: The workflow names are returned by get_workflow_names_from_file_contents
    :Then: The correct workflow name is returned
    """
    # Given
    contents = [contents_one_workflow]
    # When
    workflow_names = get_workflow_names_from_file_contents(contents)
    # Then
    assert workflow_names == [
        workflow_name_from_details(
            input_data="input", output_data="output", category="category", detail="detail"
        )
    ]


def test_get_workflow_names_from_files_multiple_workflows(
    contents_one_workflow, contents_two_workflows
):
    """
    :Given: Two code files with multiple workflows
    :When: The workflow names are returned by get_workflow_names_from_file_contents
    :Then: The correct workflow names are returned
    """
    # Given
    contents = [contents_one_workflow, contents_two_workflows]
    # When
    workflow_names = get_workflow_names_from_file_contents(contents)
    # Then
    assert workflow_names == [
        workflow_name_from_details(
            input_data="input", output_data="output", category="category", detail="detail"
        ),
        workflow_name_from_details(
            input_data="input1", output_data="output1", category="category1", detail="detail1"
        ),
        workflow_name_from_details(
            input_data="input2", output_data="output2", category="category2", detail="detail2"
        ),
    ]


def test_workflow_missing_from_index():
    """
    :Given: A list of workflow names and the contents of an index file that is missing a workflow
    :When: Calling workflow_missing_from_index
    :Then: The function returns True
    """
    # Given
    workflow_names = ["workflow1", "workflow2"]
    index_content = """.. toctree::
    :maxdepth: 2
    caption: Contents:

    workflow1
    workflow2
"""
    # When
    result = workflow_missing_from_index(workflow_names, index_content)
    # Then
    assert result is False

    # and Given
    index_content_missing = """.. toctree::
    :maxdepth: 2
    :caption: Contents:

    workflow1
    """
    # When
    result = workflow_missing_from_index(workflow_names, index_content_missing)
    # Then
    assert result is True


def test_workflow_documentation_missing():
    """
    :Given: A list of workflow names and a list of documentation files
    :When: Calling workflow_documentation_missing
    :Then: The function returns False if all workflows have documentation files and True if any are missing
    """
    # Given
    workflow_names = ["workflow1", "workflow2"]
    doc_files = ["workflow1.rst", "workflow2.rst"]
    # When
    result = workflow_documentation_missing(workflow_names, doc_files)
    # Then
    assert result is False

    # and Given
    doc_files_missing = ["workflow1.rst"]
    # When
    result = workflow_documentation_missing(workflow_names, doc_files_missing)
    # Then
    assert result is True


@pytest.fixture
def create_temp_files(tmpdir):
    """
    Fixture to create temporary files for testing various scenarios with main().

    Creates two python files with Workflow objects, a docs directory with an index.rst file and two documentation files.
    """
    workflow1_py = tmpdir.join("workflow1.py")
    workflow2_py = tmpdir.join("workflow2.py")
    docs_dir = tmpdir.mkdir("docs")
    index_rst = docs_dir.join("index.rst")
    doc_file1 = docs_dir.join("input1_to_output1_category1_detail1.rst")
    doc_file2 = docs_dir.join("input2_to_output2_category2_detail2.rst")

    workflow1_py.write(
        """_ = Workflow(
    input_data="input1",
    output_data="output1",
    category="category1",
    detail="detail1"
)"""
    )
    workflow2_py.write(
        """_ = Workflow(
    input_data="input2",
    output_data="output2",
    category="category2",
    detail="detail2"
)"""
    )
    index_rst.write(
        """.. toctree::
    :maxdepth: 2
    :caption: Contents:

    input1_to_output1_category1_detail1
    input2_to_output2_category2_detail2"""
    )
    doc_file1.write("Documentation for workflow1")
    doc_file2.write("Documentation for workflow2")

    return tmpdir


def test_calibration_workflow_name_hook_success(create_temp_files):
    """
    :Given: Two python files with Workflow objects, a docs directory with an index.rst file and two documentation files
    :When: Calling calibration_workflow_name_hook
    :Then: The function returns 0
    """
    # Given
    temp_dir = create_temp_files
    filenames = [str(temp_dir.join("workflow1.py")), str(temp_dir.join("workflow2.py"))]
    original_cwd = os.getcwd()
    os.chdir(temp_dir)
    # When / Then
    try:
        assert calibration_workflow_name_hook(filenames) == 0
    finally:
        os.chdir(original_cwd)


def test_calibration_workflow_name_hook_missing_docs_dir(tmpdir):
    """
    :Given: A python file with a Workflow object and no docs directory
    :When: Calling calibration_workflow_name_hook
    :Then: The function returns 1
    """
    # Given
    workflow1_py = tmpdir.join("workflow1.py")
    workflow1_py.write(
        """_ = Workflow(
    input_data="input1",
    output_data="output1",
    category="category1",
    detail="detail1"
)"""
    )
    filenames = [str(workflow1_py)]
    original_cwd = os.getcwd()
    os.chdir(tmpdir)
    try:
        assert calibration_workflow_name_hook(filenames) == 1
    finally:
        os.chdir(original_cwd)


def test_calibration_workflow_name_hook_missing_index_entry(create_temp_files):
    """
    :Given: An index file missing an entry for a workflow
    :When: Calling calibration_workflow_name_hook
    :Then: The function returns 1
    """
    # Given
    temp_dir = create_temp_files
    docs_dir = temp_dir.join("docs")
    index_rst = docs_dir.join("index.rst")
    index_rst.write(
        """.. toctree::
    :maxdepth: 2
    :caption: Contents:

    input1_output1_category1_detail1"""
    )
    filenames = [str(temp_dir.join("workflow1.py")), str(temp_dir.join("workflow2.py"))]
    original_cwd = os.getcwd()
    os.chdir(temp_dir)
    # When / Then
    try:
        assert calibration_workflow_name_hook(filenames) == 1
    finally:
        os.chdir(original_cwd)


def test_calibration_workflow_name_hook_missing_doc_file(create_temp_files):
    """
    :Given: A documentation file missing for a workflow
    :When: Calling calibration_workflow_name_hook
    :Then: The function returns 1
    """
    # Given
    temp_dir = create_temp_files
    docs_dir = temp_dir.join("docs")
    doc_file2 = docs_dir.join("input2_to_output2_category2_detail2.rst")
    doc_file2.remove()
    filenames = [str(temp_dir.join("workflow1.py")), str(temp_dir.join("workflow2.py"))]
    original_cwd = os.getcwd()
    os.chdir(temp_dir)
    # When / Then
    try:
        assert calibration_workflow_name_hook(filenames) == 1
    finally:
        os.chdir(original_cwd)


def test_calibration_workflow_name_missing_index_rst(create_temp_files):
    """
    :Given: A docs directory with no index.rst file
    :When: Calling calibration_workflow_name_hook
    :Then: The function returns 1
    """
    # Given
    temp_dir = create_temp_files
    docs_dir = temp_dir.join("docs")
    index_rst = docs_dir.join("index.rst")
    index_rst.remove()
    filenames = [str(temp_dir.join("workflow1.py")), str(temp_dir.join("workflow2.py"))]
    original_cwd = os.getcwd()
    os.chdir(temp_dir)
    # When / Then
    try:
        assert calibration_workflow_name_hook(filenames) == 1
    finally:
        os.chdir(original_cwd)
