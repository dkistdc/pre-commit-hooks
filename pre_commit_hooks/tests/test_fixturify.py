import pytest

from pre_commit_hooks.dkistdc_test_formatter import fixturify


def test_valid():
    """
    :Given: Source code with fixtures and no documentation errors
    :When: Validated
    :Then: Return code is 0
    """
    assert (
        fixturify(
            '''\
"""Initial docstring""""

@pytest.fixture
def fixture_1():
    """
    Fixture docstring exists
    """"
    return 1

@pytest.mark.skip
def test_not_a_fixture():
    assert True

@pytest.fixture(scope="function")
def scoped_fixture():
    """foo"""
    assert True

#@pytest.fixture
# def commented_commented_out_fixture
'''
        )
        == 0
    )


def test_no_fixtures():
    """
    :Given: Source code with no fixtures
    :When: The source is parsed
    :Then: An error code is returned
    """
    assert (
        fixturify(
            """\
import foo

def test_1():
    assert True
"""
        )
        == 0
    )


def test_first_fixture_has_no_docstring():
    """
    :Given: Source code with the first fixture missing a docstring
    :When: The source is parsed
    :Then: An error code is returned
    """
    assert (
        fixturify(
            '''\
@pytest.fixture
def no_doc_fixture():
    return 1

@pytest.fixture
def docced_fixture():
    """foo"""
'''
        )
        == 1
    )


def test_last_fixture_method_has_no_docstring():
    """
    :Given: Source code with the last fixture missing a docstring
    :When: The source is parsed
    :Then: An error code is returned

    """
    assert fixturify(
        '''\
@pytest.fixture
def docced_fixture():
    """foo"""
    return 1

@pytest.fixture
def no_doc_fixture():
    return 1
'''
    )
