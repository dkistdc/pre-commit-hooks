pre-commit-hooks
================
A set of python pre-commit hooks for use by the dkist libraries.

Hooks defined in this library
-----------------------------
The hooks defined here currently include the following:


calibration_workflow_name
   This hook tests the workflows defined in an instrument's workflows module to ensure they all have corresponding
   documentation rst files in the docs folder. If a documentation file is missing for a particular workflow, the
   hook creates a stub document file with some generic text and a command to create the workflow graphviz diagram.

dkistdc-test-formatter
   This hook runs the dkistdc test formatter on all python files in the repository. The formatter is a custom
   formatter that enforces a consistent style across all tests in the dkistdc-testing repository.


How to create a new hook
------------------------
1. Read the discussion on creating new hooks in https://pre-commit.com/#new-hooks carefully.

2. Add definition parameters for the new hook to the file .pre-commit-hooks.yaml. See the pre-commit website
   for a complete list and definitions of the various parameter fields.

3. Add a file defining the hook to the repository. The hook must implement a main() method
   and call the main method from the outermost scope of the file using the standard construct:

   .. code-block:: python

       if __name__ == "__main__":
           sys.exit(main())

   The main() method must return 0 on success, or or 1-255 on failure or if files are modified.

See existing hooks defined in this library as examples and for further information.

How to add a hook to a client library
-------------------------------------
To use a hook in a client library, you must add a new repo definition to the existing .pre-commit-config.yaml file.
An example is shown below:

.. code-block:: yaml

    - repo: https://bitbucket.org/dkistdc/pre-commit-hooks
      rev: v0.1
      hooks:
      -   id: calibration-workflow-name

You must supply a URL for the repository where the hook may be downloaded. The rev field must be the value of a tag or
a commit uuid. Finally, you must define the hooks to be used from this library using their id names.
